﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using PurchaseOrderLoad.PurchaseOrder;

namespace PurchaseOrderLoad
{
    public partial class General
    {
        private void Hoja1_Startup(object sender, System.EventArgs e)
        {
        }

        private void Hoja1_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.Startup += new System.EventHandler(this.Hoja1_Startup);
            this.Shutdown += new System.EventHandler(this.Hoja1_Shutdown);

        }

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            Excel.Worksheet headerSheet = ((Excel.Worksheet)this.Application.ActiveWorkbook.Sheets[1]);
            Excel.Range rng = headerSheet.Range["A6:O100000"];


            Excel.Worksheet itemSheet = ((Excel.Worksheet)this.Application.ActiveWorkbook.Sheets[2]);


            Excel.Range item = itemSheet.Range["A2:M100000"];

            Excel.Range outMessageMain = headerSheet.get_Range("J6");


            List<Excel.Range> listaHeaders = new List<Microsoft.Office.Interop.Excel.Range>();
            List<Excel.Range> listaItems = new List<Microsoft.Office.Interop.Excel.Range>();

            foreach (Excel.Range it in item.Rows)
            {

                listaItems.Add(it);

            }

            foreach (Excel.Range hdr in rng.Rows)
            {

                listaHeaders.Add(hdr);

            }

            var filterheaders = listaHeaders.Where(n => n.Range["A1"].Value2 != null);

            outMessageMain.Value2 += "\nProcessing headers";

            var totalHeaders = filterheaders.Count();

            var counter = 1;
            foreach (Excel.Range c in filterheaders)
            {

                //Web service instance


                outMessageMain.Value2 = "Processing: " + counter.ToString() + "/" + totalHeaders.ToString();


                PurchaseOrder.PurchaseOrderBundleMaintainRequestMessage_sync request = new PurchaseOrderBundleMaintainRequestMessage_sync();



                request.PurchaseOrderMaintainBundle = new PurchaseOrderMaintainRequestBundle[1];
                request.PurchaseOrderMaintainBundle[0] = new PurchaseOrderMaintainRequestBundle();

                if (cmbAction.Text == "Creation")
                {
                    request.PurchaseOrderMaintainBundle[0].actionCode = ActionCode.Item01;
                }

                if(cmbAction.Text == "Modification")
                {
                    request.PurchaseOrderMaintainBundle[0].actionCode = ActionCode.Item02;

                }
                request.PurchaseOrderMaintainBundle[0].BusinessTransactionDocumentTypeCode = new BusinessTransactionDocumentTypeCode();
                request.PurchaseOrderMaintainBundle[0].BusinessTransactionDocumentTypeCode.Value = "001";




                var orderID = "";
                var supplier = "";
                var buyerResponsible = "";
                var company = "";
                var billTo = "";
                var currency = "";
                var incoterms = "";
                var incotermsLocation = "";
                var paymentTerms = "";


                if (c.Range["A1"].Value2 != null)
                {

                    orderID = c.Range["A1"].Value2.ToString();

                    /*
                    request.SalesOrder[0].ID = new BusinessTransactionDocumentID();
                    request.SalesOrder[0].ID.Value = orderID;
                    */

                }

                if (c.Range["B1"].Value2 != null)
                {

                    supplier = c.Range["B1"].Value2.ToString();
                    request.PurchaseOrderMaintainBundle[0].SellerParty = new PurchaseOrderMaintainRequestBundleParty();
                    request.PurchaseOrderMaintainBundle[0].SellerParty.PartyKey = new PartyKey();
                    request.PurchaseOrderMaintainBundle[0].SellerParty.PartyKey.PartyID = new PartyID();
                    request.PurchaseOrderMaintainBundle[0].SellerParty.PartyKey.PartyID.Value = supplier;


                }

                if (c.Range["C1"].Value2 != null)
                {

                    buyerResponsible = c.Range["C1"].Value2.ToString();
                    request.PurchaseOrderMaintainBundle[0].EmployeeResponsibleParty = new PurchaseOrderMaintainRequestBundleParty();
                    request.PurchaseOrderMaintainBundle[0].EmployeeResponsibleParty.PartyKey = new PartyKey();
                    request.PurchaseOrderMaintainBundle[0].EmployeeResponsibleParty.PartyKey.PartyID = new PartyID();
                    request.PurchaseOrderMaintainBundle[0].EmployeeResponsibleParty.PartyKey.PartyID.Value = buyerResponsible;




                }

                if (c.Range["D1"].Value2 != null)
                {

                    company = c.Range["D1"].Value2.ToString();
                    request.PurchaseOrderMaintainBundle[0].BuyerParty = new PurchaseOrderMaintainRequestBundleParty();
                    request.PurchaseOrderMaintainBundle[0].BuyerParty.PartyKey = new PartyKey();
                    request.PurchaseOrderMaintainBundle[0].BuyerParty.PartyKey.PartyID = new PartyID();
                    request.PurchaseOrderMaintainBundle[0].BuyerParty.PartyKey.PartyID.Value = company;


                }

                if (c.Range["E1"].Value2 != null)
                {

                    billTo = c.Range["E1"].Value2.ToString();
                    request.PurchaseOrderMaintainBundle[0].BillToParty = new PurchaseOrderMaintainRequestBundleParty();
                    request.PurchaseOrderMaintainBundle[0].BillToParty.PartyKey = new PartyKey();
                    request.PurchaseOrderMaintainBundle[0].BillToParty.PartyKey.PartyID = new PartyID();
                    request.PurchaseOrderMaintainBundle[0].BillToParty.PartyKey.PartyID.Value = billTo;


                }

                if (c.Range["F1"].Value2 != null)
                {

                    currency = c.Range["F1"].Value2.ToString();
                    request.PurchaseOrderMaintainBundle[0].CurrencyCode = currency;

                }

                if (c.Range["G1"].Value2 != null)
                {

                    incoterms = c.Range["G1"].Value2.ToString();
                    request.PurchaseOrderMaintainBundle[0].DeliveryTerms = new PurchaseOrderMaintainRequestBundleDeliveryTerms();
                    request.PurchaseOrderMaintainBundle[0].DeliveryTerms.ActionCode = ActionCode.Item01;
                    request.PurchaseOrderMaintainBundle[0].DeliveryTerms.IncoTerms = new Incoterms();
                    request.PurchaseOrderMaintainBundle[0].DeliveryTerms.IncoTerms.ClassificationCode = incoterms;

                }

                if (c.Range["H1"].Value2 != null)
                {

                    incotermsLocation = c.Range["H1"].Value2.ToString();
                    request.PurchaseOrderMaintainBundle[0].DeliveryTerms.IncoTerms.TransferLocationName = incotermsLocation;

                }

                if (c.Range["I1"].Value2 != null)
                {

                    paymentTerms = c.Range["I1"].Value2.ToString();
                    request.PurchaseOrderMaintainBundle[0].CashDiscountTerms = new PurchaseOrderMaintenanceCashDiscountTerms();
                    request.PurchaseOrderMaintainBundle[0].CashDiscountTerms.ActionCode = ActionCode.Item01;
                    request.PurchaseOrderMaintainBundle[0].CashDiscountTerms.Code = new CashDiscountTermsCode();
                    request.PurchaseOrderMaintainBundle[0].CashDiscountTerms.Code.Value = paymentTerms;


                }

                //Get items from 

                var notNullItems = listaItems.Where(n => n.Range["A1"].Value2 != null);

                var filterItems = notNullItems.Where(n => n.Range["A1"].Value2.ToString() == orderID);


                var totalItems = filterItems.Count();

                request.PurchaseOrderMaintainBundle[0].Item = new PurchaseOrderMaintainRequestBundleItem[totalItems];
                

                var counterItems = 0;
                foreach (Excel.Range range in filterItems)
                {
                    request.PurchaseOrderMaintainBundle[0].Item[counterItems] = new PurchaseOrderMaintainRequestBundleItem();
                    request.PurchaseOrderMaintainBundle[0].Item[counterItems].actionCode = ActionCode.Item01;
                    

                    var itemOrderID = "";
                    var lineID = "";
                    var product = "";
                    var grossPrice = "";
                    var grossQuantity = "";
                    var grossUnit = "";
                    var deliveryDate = "";
                    var poa = "";
                    var gas = "";
                    var ie = "";
                    var productType = "";
                    var shipTo = "";
                    var quantity = "";
                    var quantityUnit = "";
                    var taxCountry = "";
                    var taxJurisdiction = "";
                    var taxCode = "";
                    var taxDate = "";


                    if (range.Range["A1"].Value2 != null)
                    {

                        itemOrderID = range.Range["A1"].Value2.ToString();

                        /*
                        request.SalesOrder[0].ID = new BusinessTransactionDocumentID();
                        request.SalesOrder[0].ID.Value = orderID;
                        */

                    }

                    if (range.Range["B1"].Value2 != null)
                    {

                        lineID = range.Range["B1"].Value2.ToString();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemID = lineID;
                      



                    }

                    if (range.Range["C1"].Value2 != null)
                    {

                        product = range.Range["C1"].Value2.ToString();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemProduct = new PurchaseOrderMaintainRequestBundleItemProduct();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemProduct.ProductKey = new ProductKey();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemProduct.ProductKey.ProductID = new ProductID();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemProduct.ProductKey.ProductID.Value = product;
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemProduct.ProductKey.ProductIdentifierTypeCode = "1";



                    }

                    if (range.Range["D1"].Value2 != null)
                    {

                        productType = range.Range["D1"].Value2.ToString();
                        // request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemProduct.ProductKey = new ProductKey();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemProduct.ProductKey.ProductTypeCode = productType;
                        



                    }

                    if (range.Range["E1"].Value2 != null)
                    {

                        grossPrice = range.Range["E1"].Value2.ToString();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ListUnitPrice = new Price();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ListUnitPrice.Amount = new Amount();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ListUnitPrice.Amount.Value = Decimal.Parse(grossPrice);
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ListUnitPrice.Amount.currencyCode = currency;




                    }

                    if (range.Range["F1"].Value2 != null)
                    {

                        grossQuantity = range.Range["F1"].Value2.ToString();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ListUnitPrice.BaseQuantity = new Quantity();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ListUnitPrice.BaseQuantity.Value = Decimal.Parse(grossQuantity);




                    }

                    if (range.Range["G1"].Value2 != null)
                    {

                        grossUnit = range.Range["G1"].Value2.ToString();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ListUnitPrice.BaseQuantity.unitCode = grossUnit;



                    }

                    if (range.Range["H1"].Value2 != null)
                    {

                        quantity = range.Range["H1"].Value2.ToString();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].Quantity = new Quantity();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].Quantity.Value = Decimal.Parse(quantity);


                    }


                    if (range.Range["I1"].Value2 != null)
                    {

                        quantityUnit = range.Range["I1"].Value2.ToString();

                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].Quantity.unitCode = quantityUnit;


                    }

                    if (range.Range["J1"].Value2 != null)
                    {
                        /*
                        deliveryDate = range.Range["H1"].Value2.ToString();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].DeliveryPeriod = new UPPEROPEN_LOCALNORMALISED_DateTimePeriod();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].DeliveryPeriod.EndDateTime = new LOCALNORMALISED_DateTime();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].DeliveryPeriod.EndDateTime.Value = DateTime.Parse(deliveryDate);
                        */

                    }

                    if (range.Range["K1"].Value2 != null)
                    {

                        shipTo = range.Range["K1"].Value2.ToString();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ProductRecipientParty = new PurchaseOrderMaintainRequestBundleItemParty();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ProductRecipientParty.PartyKey = new PartyKey();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ProductRecipientParty.PartyKey.PartyID = new PartyID();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ProductRecipientParty.PartyKey.PartyID.Value = shipTo;
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ShipToLocation = new PurchaseOrderMaintainRequestBundleItemLocation();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ShipToLocation.LocationID = new LocationID();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ShipToLocation.LocationID.Value = shipTo;



                    }

                    


                    if (range.Range["L1"].Value2 != null)
                    {

                        poa = range.Range["L1"].Value2.ToString();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].FollowUpPurchaseOrderConfirmation = new ProcurementDocumentItemFollowUpPurchaseOrderConfirmation();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].FollowUpPurchaseOrderConfirmation.RequirementCode = new FollowUpBusinessTransactionDocumentRequirementCode();

                        if (Boolean.Parse(poa)) {
                            request.PurchaseOrderMaintainBundle[0].Item[counterItems].FollowUpPurchaseOrderConfirmation.RequirementCode = FollowUpBusinessTransactionDocumentRequirementCode.Item01;
                        }
                        else {
                            request.PurchaseOrderMaintainBundle[0].Item[counterItems].FollowUpPurchaseOrderConfirmation.RequirementCode = FollowUpBusinessTransactionDocumentRequirementCode.Item04;

                        }



                    }

                    if (range.Range["M1"].Value2 != null)
                    {
                        gas = range.Range["M1"].Value2.ToString();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].FollowUpDelivery = new ProcurementDocumentItemFollowUpDelivery();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].FollowUpDelivery.RequirementCode = new FollowUpBusinessTransactionDocumentRequirementCode();

                        if (Boolean.Parse(gas))
                        {
                            request.PurchaseOrderMaintainBundle[0].Item[counterItems].FollowUpDelivery.RequirementCode = FollowUpBusinessTransactionDocumentRequirementCode.Item01;
                        }
                        else
                        {
                            request.PurchaseOrderMaintainBundle[0].Item[counterItems].FollowUpDelivery.RequirementCode = FollowUpBusinessTransactionDocumentRequirementCode.Item04;
                        }
                    }

                    if (range.Range["N1"].Value2 != null)
                    {

                        ie = range.Range["N1"].Value2.ToString();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].FollowUpInvoice = new ProcurementDocumentItemFollowUpInvoice();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].FollowUpInvoice.RequirementCode = new FollowUpBusinessTransactionDocumentRequirementCode();

                        if (Boolean.Parse(ie))
                        {
                            request.PurchaseOrderMaintainBundle[0].Item[counterItems].FollowUpInvoice.RequirementCode = FollowUpBusinessTransactionDocumentRequirementCode.Item01;
                        }
                        else
                        {
                            request.PurchaseOrderMaintainBundle[0].Item[counterItems].FollowUpInvoice.RequirementCode = FollowUpBusinessTransactionDocumentRequirementCode.Item04;
                        }


                    }

                    if (range.Range["O1"].Value2 != null)
                    {

                        taxCountry = range.Range["O1"].Value2.ToString();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemTaxCalculation = new PurchaseOrderMaintainRequestTaxCalculationItem();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemTaxCalculation.actionCode = ActionCode.Item01;
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemTaxCalculation.CountryCode = taxCountry;

                    }

                    if (range.Range["P1"].Value2 != null)
                    {

                        taxJurisdiction = range.Range["Q1"].Value2.ToString();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemTaxCalculation.TaxJurisdictionCode = new TaxJurisdictionCode();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemTaxCalculation.TaxJurisdictionCode.listID = taxCountry;
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemTaxCalculation.TaxJurisdictionCode.Value = taxJurisdiction;
                    }

                    if (range.Range["Q1"].Value2 != null)
                    {

                        taxCode = range.Range["Q1"].Value2.ToString();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemTaxCalculation.TaxationCharacteristicsCode = new ProductTaxationCharacteristicsCode();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemTaxCalculation.TaxationCharacteristicsCode.Value = taxCode;
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemTaxCalculation.TaxationCharacteristicsCode.listID = taxCountry;


                    }


                    if (range.Range["R1"].Value2 != null)
                    {

                        taxDate = range.Range["R1"].Value2.ToString();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemTaxCalculation.temTaxationTerms = new PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemTaxationTerms();
                        request.PurchaseOrderMaintainBundle[0].Item[counterItems].ItemTaxCalculation.temTaxationTerms.TaxDate = DateTime.Parse(taxDate);

                    }







                    counterItems++;


                }


                PurchaseOrderMaintainConfirmationBundleMessage_sync response = CallWSTST(request);





            }

            outMessageMain.Value2 += "\nFinished, check output tab for further details";




        }

        public PurchaseOrderMaintainConfirmationBundleMessage_sync CallWSTST(PurchaseOrderBundleMaintainRequestMessage_sync request)
        {

            Excel.Worksheet outSheet = ((Excel.Worksheet)this.Application.ActiveWorkbook.Sheets[4]);
            Excel.Range outMessage = outSheet.get_Range("A1");

            Excel.Worksheet confSheet = ((Excel.Worksheet)this.Application.ActiveWorkbook.Sheets[3]);
            Excel.Range URL = confSheet.get_Range("B1");
            Excel.Range serviceUser = confSheet.get_Range("B2");
            Excel.Range servicePassword = confSheet.get_Range("B3");

            String sURL = "";
            String sUser = "";
            String sPassword = "";

            if (URL.Value2 != null)
            {

                sURL = URL.Value2.ToString();
            }

            if (serviceUser.Value2 != null)
            {

                sUser = serviceUser.Value2.ToString();
            }

            if (servicePassword.Value2 != null)
            {

                sPassword = servicePassword.Value2.ToString();
            }





            service srv = new service();


            if (sPassword != "" && sUser != "" && sURL != "")
            {
                srv.Url = srv.Url.Replace("my342987.sapbydesign.com", sURL);

                srv.Credentials = new System.Net.NetworkCredential(sUser, sPassword);


                PurchaseOrderMaintainConfirmationBundleMessage_sync response = srv.ManagePurchaseOrderInMaintainBundle(request);

                if (response.PurchaseOrder != null)
                {
                    foreach (var returnMessage in response.PurchaseOrder)
                    {

                        outMessage.Value2 += "\nPurchase Order: " + returnMessage.BusinessTransactionDocumentID.Value + " was created!";

                        button1.Text = "Finished";


                    }
                }
                else
                {
                    outMessage.Value2 += "\nAn error occurred";
                }


                if (response.Log.Item != null)
                {

                    outMessage.Value2 += "\nLog Messages returned: " + response.Log.Item.Count();



                    foreach (var trag in response.Log.Item.Where(n => n.SeverityCode == "3"))
                    {

                        outMessage.Value2 += "\nSeverity Error";
                        outMessage.Value2 += "\n" + trag.Note;
                        outMessage.Value2 += "\n" + trag.NoteTemplateText;

                    }

                    foreach (var log in response.Log.Item.Where(n => n.SeverityCode != "3"))
                    {
                        outMessage.Value2 += "\nOther Messages";
                        outMessage.Value2 += "\n" + log.Note;
                        outMessage.Value2 += "\n" + log.NoteTemplateText;

                    }

                }


                return response;
            }

            else
            {
                button1.Text = "Error.. Try again";
                outMessage.Value2 += "\nAuthentication error, please check URL, User and Password located under the configuration Tab.";
                return new PurchaseOrderMaintainConfirmationBundleMessage_sync();
            }

        }



    }
}

